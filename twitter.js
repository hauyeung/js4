//instantiate XHR object
var xhr = new XMLHttpRequest();

//get response when data is received from server
xhr.onreadystatechange = function(){
	twt = document.getElementById('tweets');
	
	if (xhr.status == 200 && xhr.readyState == xhr.DONE)
	{		
		respstr = xhr.responseText;
		twts = JSON.parse(respstr);
		
		//add tweets to div
		for (i = 0; i < twts.length; i++)
		{
			tt = document.createElement('li');
			tt.innerHTML = twts[i].user.name+': '+twts[i].text
			twt.appendChild(tt);
		}
	}
	
};

//make request
xhr.open('GET','twitterJSON.json');
xhr.send();